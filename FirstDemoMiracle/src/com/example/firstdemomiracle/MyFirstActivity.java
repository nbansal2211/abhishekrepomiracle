package com.example.firstdemomiracle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyFirstActivity extends Activity implements OnClickListener {

	Button btnB;
	Button btnC;
	EditText text;
	TextView tv;
	String TAG = "Current Method Activity Main";
	int counter = 0;

	private final int reqCodeB = 1, reqCodeC = 2;

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		setContentView(R.layout.first);
		tv = (TextView) findViewById(R.id.firstTextView);
		btnB = (Button) findViewById(R.id.buttonB);
		btnC = (Button) findViewById(R.id.buttonC);
		text = (EditText) findViewById(R.id.firstEditText);
		
		if(b != null){
			counter = b.getInt("Counter");
		}
		tv.setText("Counter: "+counter);
		btnB.setOnClickListener(this);
		btnC.setOnClickListener(this);
		text.setOnClickListener(this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle b) {
		// TODO Auto-generated method stub
		b.putInt("Counter", counter);
		super.onSaveInstanceState(b);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub

		int id = view.getId();
		switch (id) {
		case R.id.buttonB:
			/*String textEntered = text.getText().toString();
			startActivity(ActivityB.class, textEntered, Constants.KeyB,
					reqCodeB);*/
			
			counter++;
			tv.setText("Counter"+counter);

			break;
		case R.id.buttonC:

			String textEnt = text.getText().toString();
			startActivity(ActivityC.class, textEnt, Constants.KeyC, reqCodeC);

			break;
		case R.id.firstTextView:
			break;
		}

	}

	private void startActivity(Class whichClass, String data, String key,
			int reqCode) {
		Intent i = new Intent(this, whichClass);
		i.putExtra(key, data);
		startActivityForResult(i, reqCode);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == reqCodeB) {
			if (resultCode == Activity.RESULT_OK) {
				String str = data.getStringExtra(Constants.KeyB);
				tv.setText(str);
			}
		}
		if (requestCode == reqCodeC) {
			if (resultCode == Activity.RESULT_OK) {
				String str = data.getStringExtra(Constants.KeyC);
				Log.e("String Retrieved Is: From Activity C", str);
				tv.setText(str);
			}
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.e(TAG, "OnStop()");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e(TAG, "OnDestroy()");
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();

		Log.e(TAG, "OnRestart()");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.e(TAG, "OnPause()");
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.e(TAG, "OnStart()");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e(TAG, "OnResunme()");
	}

}
