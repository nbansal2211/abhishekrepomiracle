package com.example.firstdemomiracle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ActivityC extends Activity {

	String TAG = "Current Method Activity C";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_c);
		Log.e(TAG, "OnCreate()");
		Intent i = new Intent();
		i.putExtra(Constants.KeyC, "ABC");
		setResult(Activity.RESULT_OK, i);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.e(TAG, "OnStop()");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e(TAG, "OnDestroy()");

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();

		Log.e(TAG, "OnRestart()");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.e(TAG, "OnPause()");

		super.onPause();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.e(TAG, "OnStart()");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e(TAG, "OnResunme()");
	}

}
